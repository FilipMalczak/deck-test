package pl.adriankozlowski;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        Collection<Card> cards = main.generateDeck();
        cards = main.shuffle(cards);
        Map<String, List<Card>> distributedCards = main.dealCards(cards);
    }

    //I will happily defend my concious decision to change return type from Collection to List
    public List<Card> shuffle(Collection<Card> cards) {
        List<Card> out = new ArrayList<>(cards);
        Collections.shuffle(out);
        return out;
    }

    public Map<String, List<Card>> dealCards(Collection<Card> cards) {
        Map<String, List<Card>> result = new HashMap<>();
        Random random = new Random();
        List<Card> shuffled = shuffle(cards);
        int dealt = 0;
        while (shuffled.size() > 2 && dealt < 13){
            for (var tile: Tile.values()){
                String tileName = tile.getTileName();
                if (!result.containsKey(tileName)){
                    result.put(tileName, new LinkedList<>());
                }
                Card chosen = shuffled.remove(random.nextInt(shuffled.size()));
                result.get(tileName).add(chosen);
                cards.remove(chosen);
            }
            dealt += 1;
        }
        return result;
    }

    public Collection<Card> generateDeck() {
        return Stream.of(Card.Value.values())
            .flatMap(value ->
                Stream.of(Card.Suit.values())
                    .map(suit ->
                        new Card(value, suit)
                    )
            )
            .collect(toList());
    }
}
