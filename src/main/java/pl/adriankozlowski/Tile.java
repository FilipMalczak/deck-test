package pl.adriankozlowski;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor
@Getter
public enum Tile {
    TILE1("tile1"),
    TILE2("tile2"),
    TILE3("tile3");

    String tileName;
}
