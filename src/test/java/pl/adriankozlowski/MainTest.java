package pl.adriankozlowski;

import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

public class MainTest {

    @Test
    public void shouldShuffle() {
        Main main = new Main();
        Collection<Card> cards = main.generateDeck();
        Collection<Card> shuffle = main.shuffle(cards);
        //fixme this test is silly - it will work if generateDeck works in any fashion (as long as it returns non-empty) and shuffle returns empty by default
        assertThat(cards).isNotSameAs(shuffle);
    }

    @Test
    public void shouldDeal13Cards() {
        Main main = new Main();
        List cards = new ArrayList();
        for (int i = 0; i < 52; i++) {
            Card card = new Card();
            card.setSuit(Card.Suit.DIAMONDS);
            card.setValue(Card.Value.FIVE);
            cards.add(card);
        }

        Map<String, List<Card>> map = main.dealCards(cards);
        for (var key : map.keySet()) {
            assertThat(map.get(key).size()).isEqualTo(13);
        }
    }

    @Test
    public void shouldDealCardsOn3Tiles() {
        Main main = new Main();
        ArrayList cards = new ArrayList();
        Card card = new Card();
        card.setSuit(Card.Suit.DIAMONDS);
        card.setValue(Card.Value.FIVE);
        cards.add(card);
        Card card1 = new Card();
        card1.setSuit(Card.Suit.DIAMONDS);
        card1.setValue(Card.Value.TWO);
        cards.add(card1);
        Card card2 = new Card();
        card2.setSuit(Card.Suit.DIAMONDS);
        card2.setValue(Card.Value.SEVEN);
        cards.add(card2);
        Card card3 = new Card();
        card3.setSuit(Card.Suit.DIAMONDS);
        card3.setValue(Card.Value.SEVEN);
        cards.add(card3);
        Map map = main.dealCards(cards);
        assertThat(map.containsKey("tile1"));
        assertThat(map.containsKey("tile2"));
        assertThat(map.containsKey("tile3"));
        assertThat(((List) map.get("tile1")).size()).isEqualTo(1);
        assertThat(((List) map.get("tile2")).size()).isEqualTo(1);
        assertThat(((List) map.get("tile3")).size()).isEqualTo(1);
        //todo consider following refactor:
        /*
        for (var tile: Tile.values()){
            assertThat(map.containsKey(tile.getTileName()));
            assertThat(((List) map.get(tile.getTileName())).size()).isEqualTo(1);
        }
         */
        assertThat(cards.size()).isEqualTo(1);
    }

    @Test
    public void shouldGenerateDeck() {
        Main main = new Main();
        Collection<Card> cards = main.generateDeck();
        assertThat(cards).areExactly(52, new Condition<Card>(card -> card != null, "karty"));
        assertThatCode(() -> {
            Arrays.stream(Card.Value.values()).forEach(value -> {
                Arrays.stream(Card.Suit.values()).forEach(suite -> {
                    assertThat(cards).areExactly(1, new Condition<Card>(card -> card.getValue().equals(value) && card.getSuit().equals(suite), value.name() + suite.name()));
                });
            });
        });
    }
}